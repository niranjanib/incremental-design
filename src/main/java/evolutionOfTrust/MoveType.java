package evolutionOfTrust;

public enum MoveType {
    CHEAT,
    COOPERATE,
    INVALID;
}
