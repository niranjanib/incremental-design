package evolutionOfTrust;

public class Machine {
    public Score play(MoveType p1Move, MoveType p2Move) {
        Score score ;
        if(p1Move == MoveType.CHEAT && p2Move == MoveType.CHEAT ){
            score = new Score(0,0);
        }
        else if(p1Move == MoveType.COOPERATE && p2Move == MoveType.COOPERATE ){
            score = new Score(2,2);
        }
        else if(p1Move == MoveType.CHEAT && p2Move == MoveType.COOPERATE ){
            score = new Score(3,-1);
        }
        else {
            score = new Score(-1,3);
        }

        return score;
    }
}
