package evolutionOfTrust;

import java.io.PrintStream;
import java.util.Scanner;

public class Game {


    private Scanner scanner;
    private final Player player1;
    private final Player player2;
    private Machine machine;
    private PrintStream printStream;

    public void play() {
        System.out.println("Enter the no of rounds  ");
        int rounds = this.scanner.nextInt();
        for(int round = 0; round<rounds;round++) {
            Score score = this.machine.play(this.player1.move(), this.player2.move());
            player1.setTotalScore(player1.getTotalScore()+score.getPlayer1Score());
            player2.setTotalScore(player2.getTotalScore()+score.getPlayer2Score());
            this.printStream.println(score);
        }
    }

    public Game(Scanner scanner, Player player1, Player player2, Machine machine, PrintStream printStream) {
        this.scanner = scanner;

        this.player1 = player1;
        this.player2 = player2;
        this.machine = machine;
        this.printStream = printStream;
    }
}
