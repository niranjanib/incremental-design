package evolutionOfTrust;

import java.util.Scanner;

public class Player {

    public Scanner scanner;

    private int totalScore;

    public Player(Scanner scanner) {
        this.scanner = scanner;
    }

    public MoveType move() {
       int moveValue = getUserInput();
       if (moveValue == 0) {
           return MoveType.CHEAT;
        } else if (moveValue == 1){
            return MoveType.COOPERATE;
        } else {
           return MoveType.INVALID;
       }
    }

    public int getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(int totalScore) {
        this.totalScore = totalScore;
    }

    private int getUserInput() {
        System.out.println("Enter MoveType(0 - CHEAT / 1- COOPERATE)");
        int moveValue = this.scanner.nextInt();
        return moveValue;
    }


}
