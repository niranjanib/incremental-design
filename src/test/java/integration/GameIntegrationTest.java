package integration;

import evolutionOfTrust.Game;
import evolutionOfTrust.Machine;
import evolutionOfTrust.Player;
import org.junit.Assert;
import org.junit.Test;

import java.io.PrintStream;
import java.util.Scanner;

public class GameIntegrationTest {
    @Test
    public void shouldDisplayScoreWhenBothPlayerCooperateForThreeRounds(){
        Player player1 = new Player(new Scanner("1 \n 1 \n 1"));
        Player player2 = new Player(new Scanner("1 \n 1 \n 1"));
        Machine machine = new Machine();
        PrintStream printStream = new PrintStream(System.out);
        Game game = new Game(new Scanner("3"),player1,player2,machine,printStream);
        game.play();
        Assert.assertEquals(6,player1.getTotalScore());
        Assert.assertEquals(6,player2.getTotalScore());

    }
}
