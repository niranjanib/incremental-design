package unit;

import evolutionOfTrust.MoveType;
import evolutionOfTrust.Player;
import org.junit.Assert;
import org.junit.Test;

import java.util.Scanner;

public class PlayerTest {

    @Test
    public void shouldReturnCheatIfInputIs0() {
        Player player = new Player(new Scanner("0"));
        Assert.assertEquals(MoveType.CHEAT,player.move());
    }

    @Test
    public void shouldReturnCheatIfInputIs1() {
        Player player = new Player(new Scanner("1"));
        Assert.assertEquals(MoveType.COOPERATE,player.move());
    }

    @Test
    public void shouldReturnInvalidForInputGreaterThan1() {
        Player player = new Player(new Scanner("2"));
        Assert.assertEquals(MoveType.INVALID,player.move());
    }

    @Test
    public void shouldUpdateTotalScoreWhenScoreIsPositive() {
        Player player = new Player(new Scanner("1"));
        player.setTotalScore(3);
        Assert.assertEquals(player.getTotalScore(),3);
    }

    @Test
    public void shouldUpdateTotalScoreWhenScoreIsNegative() {
        Player player = new Player(new Scanner("0"));
        player.setTotalScore(-1);
        Assert.assertEquals(player.getTotalScore(),-1);
    }



}