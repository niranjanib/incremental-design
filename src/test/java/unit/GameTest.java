package unit;

import evolutionOfTrust.*;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.PrintStream;
import java.util.Scanner;

@RunWith(MockitoJUnitRunner.class)
public class GameTest {

    @InjectMocks
    Game game;

    @Mock
    Player player1;

    @Mock
    Player player2;

    @Mock
    Machine machine;

    @Mock
    PrintStream printStream;

    @Captor
    ArgumentCaptor<Score> scoreCaptor;

    @Test
    public void shouldDisplayBoth2ScoreWhenBothPlayersCooperate(){

        Score expectedScore = new Score(2,2);

        Mockito.when(player1.move()).thenReturn(MoveType.COOPERATE);
        Mockito.when(player2.move()).thenReturn(MoveType.COOPERATE);
        Mockito.when(machine.play(MoveType.COOPERATE, MoveType.COOPERATE)).thenReturn(expectedScore);

        game = new Game(new Scanner("2"),player1, player2, machine,printStream);
        game.play();

        Mockito.verify(player1,Mockito.times(2)).move();
        Mockito.verify(player1,Mockito.times(2)).move();
        Mockito.verify(player1,Mockito.times(2)).setTotalScore(2);
        Mockito.verify(player1,Mockito.times(2)).setTotalScore(2);

        Mockito.verify(player2,Mockito.times(2)).move();

        Mockito.verify(machine, Mockito.times(2)).play(MoveType.COOPERATE, MoveType.COOPERATE);

        Mockito.verify(printStream, Mockito.times(2)).println(scoreCaptor.capture());

        Assert.assertEquals(expectedScore, scoreCaptor.getValue());
    }

    @Test
    public void shouldDisplayBoth0ScoreWhenBothPlayersCheat(){

        Score expectedScore = new Score(0,0);

        Mockito.when(player1.move()).thenReturn(MoveType.CHEAT);
        Mockito.when(player2.move()).thenReturn(MoveType.CHEAT);
        Mockito.when(machine.play(MoveType.CHEAT, MoveType.CHEAT)).thenReturn(expectedScore);

        Game game = new Game(new Scanner("2"),player1, player2, machine,printStream);
        game.play();

        Mockito.verify(player1,Mockito.times(2)).move();
        Mockito.verify(player2,Mockito.times(2)).move();

        Mockito.verify(machine, Mockito.times(2)).play(MoveType.CHEAT, MoveType.CHEAT);

        Mockito.verify(printStream, Mockito.times(2)).println(scoreCaptor.capture());
        Assert.assertEquals(expectedScore, scoreCaptor.getValue());
    }

    @Test
    public void shouldDisplayDifferentScoreWhenPlayer1CooperatePlayer2Cheats(){

        Score expectedScore = new Score(-1,3);

        Mockito.when(player1.move()).thenReturn(MoveType.COOPERATE);
        Mockito.when(player2.move()).thenReturn(MoveType.CHEAT);
        Mockito.when(machine.play(MoveType.COOPERATE, MoveType.CHEAT)).thenReturn(expectedScore);

        Game game = new Game(new Scanner("3"),player1, player2, machine,printStream);
        game.play();

        Mockito.verify(player1,Mockito.times(3)).move();
        Mockito.verify(player2,Mockito.times(3)).move();

        Mockito.verify(machine, Mockito.times(3)).play(MoveType.COOPERATE, MoveType.CHEAT);

        Mockito.verify(printStream, Mockito.times(3)).println(scoreCaptor.capture());
        Assert.assertEquals(expectedScore, scoreCaptor.getValue());
    }

    @Test
    public void shouldDisplayDifferentScoreWhenPlayer1CheatsPlayer2Cooperate(){

        Score expectedScore = new Score(3,-1);

        Mockito.when(player1.move()).thenReturn(MoveType.CHEAT);
        Mockito.when(player2.move()).thenReturn(MoveType.COOPERATE);
        Mockito.when(machine.play(MoveType.CHEAT, MoveType.COOPERATE)).thenReturn(expectedScore);

        Game game = new Game(new Scanner("2"),player1, player2, machine,printStream);
        game.play();

        Mockito.verify(player1,Mockito.times(2)).move();
        Mockito.verify(player2,Mockito.times(2)).move();

        Mockito.verify(machine, Mockito.times(2)).play(MoveType.CHEAT, MoveType.COOPERATE);

        Mockito.verify(printStream, Mockito.times(2)).println(scoreCaptor.capture());
        Assert.assertEquals(expectedScore, scoreCaptor.getValue());
    }
}
