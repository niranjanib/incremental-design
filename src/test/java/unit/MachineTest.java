package unit;

import evolutionOfTrust.MoveType;
import evolutionOfTrust.Machine;
import evolutionOfTrust.Score;
import org.junit.Assert;
import org.junit.Test;

public class MachineTest {

    Machine machine = new Machine();

    @Test
    public void playWhenBothCheat(){
        Score score = machine.play(MoveType.CHEAT, MoveType.CHEAT);
        Assert.assertEquals(0, score.getPlayer1Score());
        Assert.assertEquals(0, score.getPlayer2Score());
    }

    @Test
    public void playWhenBothCooperate(){
        Score score = machine.play(MoveType.COOPERATE, MoveType.COOPERATE );
        Assert.assertEquals(2, score.getPlayer1Score());
        Assert.assertEquals(2, score.getPlayer2Score());
    }

    @Test
    public void playWhenP1CheatsAndP2Cooperates(){
        Score score = machine.play(MoveType.CHEAT, MoveType.COOPERATE );
        Assert.assertEquals(3, score.getPlayer1Score());
        Assert.assertEquals(-1, score.getPlayer2Score());
    }

    @Test
    public void playWhenP1CooperatesAndP2Cheats(){
        Score score = machine.play(MoveType.COOPERATE, MoveType.CHEAT );
        Assert.assertEquals(-1, score.getPlayer1Score());
        Assert.assertEquals(3, score.getPlayer2Score());
    }
}
